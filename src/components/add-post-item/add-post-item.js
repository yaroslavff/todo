import {ADD_POST} from '../../redux/actions';
import React, {Component} from 'react';
import { toggleAddPostPanel } from '../add-post/add-post';
import { connect } from 'react-redux';
import { randomKey } from '../../App';

export class AddPostItem extends Component {

  constructor(props) {
    super(props);
    this.onAddPosts = this.props.onAddPosts;
  }

  addPost = (e) => {
    e.preventDefault();
    this.onAddPosts(this.postAreaValue.value);
    this.postAreaValue.value = "";
    toggleAddPostPanel();
  }

  render() {
    let input = <textarea className="add-post-panel__input container" ref={(textarea) => this.postAreaValue = textarea} ></textarea>

    return (
      <form
        className="add-post-panel__content container"
        onSubmit={this.addPost}
      >
        {input}
        <div className="add-post-panel__btn-group container">
          <input
            type="button"
            className="add-post-panel__btn-close add-post-btn"
            value="Закрыть"
            onClick={toggleAddPostPanel}
          />
          <input
            type="submit" 
            className="add-post-panel__btn-save add-post-btn"
            value="Сохранить"
          />
        </div>
      </form>
    )
  }
}

export default connect(
  state => ({
    store: state
  }),
  dispatch => ({
    onAddPosts: (post) => {
      dispatch({type: ADD_POST, payload: {id: randomKey(), text: post}})
    }
  })
)(AddPostItem);