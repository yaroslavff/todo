import PostListItem from "../post-list-item/post-list-item";

export default function PostList(props) {

  const arr = props.store;

  const posts = arr.map(item => {
    if (item.id) return <PostListItem item={item.text} key={item.id} id={item.id}/>
  })

  return (
    <main className="container">
      <ul className="post-list">
        {posts}
      </ul>
    </main>
  )
}