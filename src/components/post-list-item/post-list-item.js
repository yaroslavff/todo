import { DELETE_POST } from "../../redux/actions";
import React, { Component } from "react";
import { connect } from "react-redux";

export class PostListItem extends Component {
  constructor(props) {
    super(props);
    this.onDeletePost = this.props.deletePost;
  }

  deletePost = (e) => {
    if(e.target.classList.contains("post-list__delete")) {
      this.onDeletePost(this.props.id);
    } else {
      e.target.classList.toggle("post-list__btn_active");
    }
  }

  render() {
    const {id, item} = this.props;

    return(
      <li className="post-list__item" id={id}>
        <div
          name="posts"
          className="post-list__btn checkbox"
          onClick={this.deletePost}
        ></div>
        <span
          className="post-list__text"
          onClick={(e) => editPost(e, item, id)}
        >{item}</span>
      </li>
    )
  }
}

export const changePosts = (e) => {
  if (e.target.textContent === "Править") {
    e.target.textContent = "Отменить"
  } else {
    e.target.textContent = "Править"
  }
  document.querySelectorAll(".checkbox").forEach((item, i) => {
    item.classList.toggle("post-list__delete");
    item.classList.toggle("post-list__btn");
    document.querySelectorAll(".post-list__text")[i].classList.toggle("post-list__text_edit");
  });
  document.querySelector(".add-post").classList.toggle("display-none");
}

const editPost = (e, editedText, editedTextId) => {
  if(e.target.classList.contains("post-list__text_edit")) {
    toggleEditPostPanel(editedText, editedTextId);
  }
}

export const toggleEditPostPanel = (defaultInputValue, inputId) => {
  if (defaultInputValue && inputId) {
    document.querySelector(".add-post-panel__input_edit").value = defaultInputValue;
    document.querySelector(".add-post-panel__input_edit").id = inputId;
  }
  const panel = document.querySelector(".edit-post-panel");
  panel.classList.toggle("display-none")
}

export default connect(
  state => ({
    store: state
  }),
  dispatch => ({
    deletePost: (postId) => {
      dispatch({type: DELETE_POST, payload: postId});
    }
  })
)(PostListItem);