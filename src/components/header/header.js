import { changePosts } from "../post-list-item/post-list-item"

export default function Header() {
  return (
    <header className="header container">
      <h1 className="header__text">Сегодня</h1>
      <button
        className="header__edit-button"
        onClick={changePosts}
      >Править</button>
    </header>
  )
}