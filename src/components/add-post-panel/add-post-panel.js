import AddPostItem from "../add-post-item/add-post-item"

export default function AddPostPanel(props) {
  return (
    <div className="add-post-panel display-none">
      <AddPostItem />
    </div>
  )
}