export default function AddPost() {

  return (
    <button
      className="add-post"
      onClick={toggleAddPostPanel}
    />
  )
}

export const toggleAddPostPanel = () => {
  const panel = document.querySelector(".add-post-panel");
  panel.classList.toggle("display-none")
}