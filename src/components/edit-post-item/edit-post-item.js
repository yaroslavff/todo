import {EDIT_POST} from '../../redux/actions';
import React, {Component} from 'react';
import { toggleEditPostPanel } from '../post-list-item/post-list-item'; 
import { connect } from 'react-redux';

export class EditPostItem extends Component {

  constructor(props) {
    super(props);
    this.onEditPosts = this.props.onEditPosts;
  }

  editPost = (e) => {
    e.preventDefault();
    this.onEditPosts(this.postInputValue.id, this.postInputValue.value);
    this.postInputValue.value = "";
    toggleEditPostPanel();
  }

  render() {


    return (
      <form
        className="add-post-panel__content container"
        onSubmit={this.editPost}
      >
      <input type="text" className="add-post-panel__input add-post-panel__input_edit container" ref={(input) => this.postInputValue = input}/>
        <div className="add-post-panel__btn-group container">
          <input
            type="button"
            className="add-post-panel__btn-close add-post-btn"
            value="Закрыть"
            onClick={toggleEditPostPanel}
          />
          <input
            type="submit" 
            className="add-post-panel__btn-save add-post-btn"
            value="Сохранить"
          />
        </div>
      </form>
    )
  }
}

export default connect(
  state => ({
    store: state
  }),
  dispatch => ({
    onEditPosts: (id, post) => {
      dispatch({type: EDIT_POST, payload: {id: id, text: post}})
    }
  })
)(EditPostItem);