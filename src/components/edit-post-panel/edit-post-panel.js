import EditPostItem from "../edit-post-item/edit-post-item"

export default function EditPostPanel(props) {
  return (
    <div className="add-post-panel edit-post-panel display-none">
      <EditPostItem />
    </div>
  )
}