import React from 'react';
import Header from './components/header/header';
import PostList from './components/post-list/post-list';
import AddPost from './components/add-post/add-post';
import AddPostPanel from './components/add-post-panel/add-post-panel';
import EditPostPanel from './components/edit-post-panel/edit-post-panel';
import {connect} from 'react-redux';

function App(props) {
  return (
    <div className="app">
      <Header />
      <PostList store={props.store}/>
      <div className="app__add-btn container">        
        <AddPost />
      </div>
      <AddPostPanel />
      <EditPostPanel />
    </div>
  );
}

export default connect(
  state => ({
    store: state
  }),
  dispatch => ({})
)(App);

export const randomKey = () => {
  let res = '';
  let vars = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
  for( let i=0; i < 9; i++ ) {
    res += vars.charAt(Math.floor(Math.random() * vars.length - 1));
  }
  return res
}
