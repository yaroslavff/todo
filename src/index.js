import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import { store } from './redux/reducer';


// const editPostReducer = (state = [], action) => {
//   switch (action.type) {
//     case DELETE_POST:
//       return state
//     case EDIT_POST:
//       return state
//     default:
//       return state
//   }
// }

// store.subscribe(() => {
//   console.log(`store: ${store.getState()}`)
// })

// store.dispatch({type: ADD_POST, payload: "aboba"})

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
