import { ADD_POST, DELETE_POST, EDIT_POST } from "./actions";
import { createStore } from "redux";
import { randomKey } from "../App";

let InitialState = [
  {id: randomKey(), text: "Побриться"},
  {id: randomKey(), text: "Купить молоко"},
  {id: randomKey(), text: "Купить сыр"}
];
const reducer = (state = InitialState, action) => {
  switch (action.type) {
    case ADD_POST:
      return [
        ...state,
        action.payload
      ]
    case DELETE_POST:
      const index = state.findIndex(item => item.id === action.payload);
      const before = state.slice(0, index);
      const after = state.slice(index + 1);
      const newState = [
        ...before,
        ...after
      ]
      return newState
    case EDIT_POST:
      const editIndex = state.findIndex(item => item.id === action.payload.id);
      const editedPost = {id: action.payload.id, text: action.payload.text}
      const editBefore = state.slice(0, editIndex);
      const editAfter = state.slice(editIndex + 1);
      const editedState = [
        ...editBefore,
        editedPost,
        ...editAfter
      ]
      return editedState
    default:
      return state
  }
}

export const store = createStore(reducer);